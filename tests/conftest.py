"""pytester is needed for testing plgugins."""


# pytest_plugins = ['pytester']

from pytest_yaml_yoyo import my_builtins
import uuid
import random


def random_user(index):
    """生成随机账号 4-16位数字+字符a-z"""
    return str(uuid.uuid4()).replace('-', '')[:index]


# 注册到内置方法
my_builtins.random_user = random_user


def func(x):
    return f"hello{x}"


my_builtins.func = func


def func_list():
    return ['test1', 'test2', 'test3']


my_builtins.func_list = func_list


def func1(req):
    print(f'请求预处理：{req}')


def func2():
    print(f'请求预处理-----------')


my_builtins.func1 = func1
my_builtins.func2 = func2


def hook_response(response, *args, **kwargs):
    # print(response.text) 原始数据
    print("执行response hook 函数内容....")
    class NewResponse:
        text = '{"code": 0, "data": {"token": "yo yo"}}'  # response.text解密
        history = response.history
        headers = response.headers
        cookies = response.cookies
        status_code = response.status_code
        raw = response.raw
        is_redirect = response.is_redirect
        content = b'{"code": 0, "data": {"token": "yo yo"}}'  # response.text解密
        elapsed = response.elapsed

        @staticmethod
        def json():
            # 拿到原始的response.json() 后解码
            return {"code": 0, "data": {"token": "yo yo"}}

    return NewResponse

my_builtins.hook_response = hook_response

